FROM node:12.21-alpine

ARG instance_name
ARG node_env
ARG server_protocol
ARG server_host
ARG server_port
ARG client_protocol
ARG client_host
ARG client_port
ARG public_client_host
ARG public_client_protocol
ARG public_client_port

ENV INSTANCE_NAME $instance_name
ENV NODE_ENV "production"
ENV SERVER_PROTOCOL "http"
ENV SERVER_HOST "localhost"
ENV SERVER_PORT "3000"
ENV CLIENT_PROTOCOL "http"
ENV CLIENT_HOST "0.0.0.0"
ENV CLIENT_PORT "4000"
ENV PUBLIC_CLIENT_HOST $public_client_host
ENV PUBLIC_CLIENT_PORT $public_client_port
ENV PUBLIC_CLIENT_PROTOCOL $public_client_protocol

RUN apk add --no-cache git python make g++ bash tar

WORKDIR /home/node/app

COPY . .
RUN chown -R node:node .
ADD ./node_modules_dev.tar.gz .
RUN mv ./node_modules_dev ./node_modules && \
    yarn webpack --config webpack/webpack.production.config.js

RUN cp -r /home/node/app/_build/assets/* /home/node/app/_build

ENTRYPOINT ["sh", "./scripts/setupProdServer.sh"]
CMD ["node", "./startServer.js"]
